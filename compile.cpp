#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

using namespace std;

string HexToBytes (const string& hexstr) {
    string bytes;
    for (size_t i = 0; i < hexstr.length(); i += 2) {
        auto bytestr = hexstr.substr(i, 2);
        bytes.push_back(strtol(bytestr.c_str(), nullptr, 16));
    }
    return bytes;
}

unsigned compile (const string& target, const string& basedir = "data\\") {
    ofstream fout;
    fout.open(target, ios::binary | ios::trunc);
    fout.close();
    fout.open(target, ios::binary | ios::app);
    int i = 0;
    uint64_t size = 0;
    for (;; ++i) {
        auto filename = to_string(i) + ".txt";
        ifstream fin(basedir + filename);
        if (!fin) break;

        string data;
        while (fin >> data) {
            fout << HexToBytes(data);
            size += data.length() / 2;
            cout << "Size: " << fixed << setprecision(3) << size / (1024. * 1024) << " MB" << '\r';
            cout.flush();
        }
        fout.flush();
    }
    return i;
}

int main () {
    auto cnt = compile("output");
    cout << "Compilation succeeded. " + to_string(cnt) + " files in total" << endl;

    return 0;
}