<!-- __loli__, you ___ ____ -->

# 猴子打字机

## 写在前面

<center><a href="https://github.com/hololiveEventCollection/hololive-20200924"><b>看这里看这里看这里</b></a>
</center>

## 介绍

本记录中的所有数据(也即[data](./data/)文件夹下的所有文件)为一次["猴子, 打字机与莎士比亚"式实验](https://en.wikipedia.org/wiki/Infinite_monkey_theorem)中的产物, 与任何现实团体或个人无关.

运行[compile.py](compile.py)或[compile.cpp](compile.cpp)所编译出的[compile.exe](compile.exe)可将数据编译为稍"美观"的形式; 同样地, 编译产物与任何现实团体或个人无关, 如有重合纯属巧合

本仓库, 其中所有数据及上述脚本对本仓库中数据之编译产物适用[CC0 1.0 Universal (CC0 1.0)协议](https://creativecommons.org/publicdomain/zero/1.0/)

<a rel="license" href="http://creativecommons.org/publicdomain/zero/1.0/"><img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" /></a>